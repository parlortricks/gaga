function _init()
    actions={}
    items={}
    fruitlist={{0,2,4,8,12,14,32,34,38},3,0}
    oddlist={{6,10,36},10,1}
    shinylist={{40,42,44,46,106},25,2}
    splash_anim={230,232,234,232,230}
    splashes={}

    to_items(fruitlist,80)
    to_items(oddlist,15)
    to_items(shinylist,5)

    fruits={}

    highscores={
        0,0,0,0,0,0,0,0,0,0
    }

    last_highscore=-1

    gravity=1
    level=1
    points=0
    playing=false
    scored=false


    --generate rock table array for cliff
    rock={204,206,236,238}
    rocks_table={}
    for x=1,16 do
        local rock_table={}
        for y=1,16 do
            add(rock_table,rock[ceil(rnd(#rock))])
        end
        add(rocks_table,rock_table)
    end

    cartdata("parlortricks_gaga_2")
    last_played=get_last_played()
    save_last_played()
    get_highscore()

    menuitem(1,"high scores", function() highscore_init() end)

    menu_init()
end

function _draw()


end

function _update()


end
    
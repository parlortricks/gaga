function over_init()
    _update=over_update
    _draw=over_draw
    
    music(-1)
    add_highscore(points)
end

function over_update()
    scored=false
    if btnp(❎) then
        menu_init()
    end
    if btnp(🅾️) then
        highscore_init()
    end
end

function over_draw()

    pal(0,133,0)
    rectfill(28,22,99,75,0)
    pal()

    palt(colors.black,false)
    palt(colors.storm, true)
    spr(160,33,24,2,2)
    spr(162,49,24,2,2)
    spr(164,65,24,2,2)
    spr(166,81,24,2,2)

    spr(168,33,42,2,2)
    spr(170,49,42,2,2)
    spr(166,65,42,2,2)
    spr(172,81,42,2,2)
    palt(colors.black,true)
    palt(colors.storm, false)


    
    local over_txt = "press \fb❎ \fafor menu"
    local over_txt_shadow = "press ❎ for menu"

    local over_txt2 = "\f8🅾️ \fafor highscore"
    local over_txt_shadow2 = "🅾️ for highscore"

    

    print(over_txt_shadow,hcenter(over_txt_shadow)-1,61,colors.black) 
    print(over_txt,hcenter(over_txt_shadow)-2,60,colors.lemon) 

    
    print(over_txt_shadow2,hcenter(over_txt_shadow2)-1,69,colors.black) 
    print(over_txt2,hcenter(over_txt_shadow2)-2,68,colors.lemon) 

    scoreboard()

end

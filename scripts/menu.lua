function menu_init()
    _update = menu_update
    _draw = menu_draw
    count=0   
    level=1
    points=0
    fade=false
    fade_count=90
    music(-1)
end

function menu_update()
    if btnp(❎) then
        fade=true
    end

    if fade and fade_count<=0 then
        music(0)
        game_init()
    end

    count+=0.04
end

function menu_draw()
    cls(3)
    draw_tv()


    palt(colors.black,false)
    palt(colors.storm, true)
    spr(160,33,40+sin(count)*2,2,2)
    spr(162,49,40+sin(count+0.2)*2,2,2)
    spr(160,65,40+sin(count+0.4)*2,2,2)
    spr(162,81,40+sin(count+0.6)*2,2,2)
    spr(96,56,90,2,2)
    palt(colors.black,true)
    palt(colors.storm, false)    
    print("\fapress \fb❎ \fato start",30,70)
    

    if year != 0 then
        local txt="last played "..last_played
        pal(0,133,0)    
        rectfill(0,121,127,127,0)
        pal()
        print(txt,hcenter(txt),122,colors.lemon)
    end

    fade_out()
    
end

function draw_tv()
    rectfill(4,23,123,86,0)
    rectfill(5,24,122,85,1)
    rectfill(9,29,118,81,0)
    pal(0,133,0)    
    rectfill(10,30,117,80,0)
    pal()
    rectfill(50,86,78,92,0)
    rectfill(51,87,77,92,1)
    rectfill(35,92,93,95,0)
    rectfill(36,93,92,94,1)
    
end

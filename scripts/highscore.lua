function highscore_init()
    _update=highscore_update
    _draw=highscore_draw
    rainbow=0
    music(-1)
end

function highscore_update()
    rainbow+=1
    if btnp(❎) or btnp(🅾️) then
        menu_init()
    end
end

function highscore_draw()
    draw_rainbow()
    pal(0,133,0)
    
    rectfill(6,2,122,22,0)
    rectfill(2,6,125,18,0)
    circfill(6,6,4,0)
    circfill(6,18,4,0)

    circfill(121,6,4,0)
    circfill(121,18,4,0)

    rectfill(6,25,122,114,0)
    rectfill(2,29,125,110,0)

    circfill(6,29,4,0)
    circfill(6,110,4,0)

    circfill(121,29,4,0)
    circfill(121,110,4,0)
    
    
    rectfill(6,117,121,125,0)
    circfill(6,121,4,0)
    circfill(121,121,4,0)
    
    
    pal()

    draw_title()
    print("press \fb❎\fa/\f8🅾️ \fafor menu",hcenter("press ❎/🅾️ for menu"),119,colors.lemon) 



    for i=1,#highscores do
        local tmp = tostr(highscores[i])
        local txt="#"..i
        local col = colors.white
        for j=1, 11-#tmp do
            txt = txt.."."
        end
        txt = txt..tmp
        local x = hcenter(txt)
        if i == 10 then x-=1 end
        if i==1 then 
            col=colors.lemon
        elseif i==2 then
            col=colors.tan
        elseif i==3 then
            col=colors.silver
        else
            col=colors.dusk
        end
        print(txt,x,(i*9)+18,col)

        
    end

end

--loads all hs from storage
function get_highscore()
    if dget(1) == 0 or dget(1) == nil then 
        highscores={2000,1000,500,100,75,50,25,15,10,5}
    else
	    for i=1, #highscores do
		    highscores[i]=dget(i)
	    end
	    last_highscore = dget(#highscores+1)
    end
end

--delete all hiscores
function delete_hightscore()
	for i=1, #highscores do
		highscores[i]=0
		dset(i,0)
	end
end

--save highscore to storage
function save_highscore()
	for i=1, #highscores do
		dset(i,highscores[i])
	end
	dset(#highscores+1,last_highscore)
end


--add new highscore, sort, store
function add_highscore(score)
	last_highscore = score
	for i=1, #highscores do
		local temp = highscores[i]
		if score > highscores[i] then
			highscores[i] = score
			score = temp
		end
	end
	save_highscore()
end

function draw_rainbow()
    --replace colours so only rainbox exists
    pal(0,8,0)
    pal(1,9,0)
    pal(2,10,0)
    pal(3,11,0)
    pal(4,12,0)
    pal(5,13,0)
    pal(6,14,0)
    pal(7,15,0)
    for y=0,127 do
        for x=0,127 do
            col=(x+y+rainbow)/4
            pset(x,y,col)
        end 
    end
    pal()   
end

function draw_title()
    palt(colors.black,false)
    palt(colors.storm, true)    
    local title_sprites={26,27,40,26,28,34,44,46,43}
    local dx = 13
    local dy = 6
    for i=1, #title_sprites do
        local sp = title_sprites[i]
        local sx,sy = (sp % 8) * 16, (sp \ 8) * 16
       
        sspr(sx,sy,14,16,i*dx-7,dy,11,13)
    end
    palt(colors.black,true)
    palt(colors.storm, false)  
end
function fruit_init()
    fruits={}
    for i=1,level do
        choice = from_items()
        local list=choice[1]
        fruit={
            sprite=list[ceil(rnd(#list))],
            x=flr(rnd(112)),
            y=i*(-16),
            gravity=rnd(1.25)+0.5,
            pscore=choice[2],
            nscore=choice[2]*2,
            sound=choice[3]            
        }
        if fruit.x > 112 then fruit.x = 112 end
        if fruit.x < 0 then fruit.x = 0 end
        add(fruits,fruit)
    end

end

function fruit_update()
    for fruit in all(fruits) do
        fruit.y+=fruit.gravity
     
      
        if hitbox_collide(fruit.x,fruit.y,fruit.x+16,fruit.y+16,player.x,player.y,player.x+16,player.y+16) then
            points+=fruit.pscore
            player.life_score+=fruit.pscore
            del(fruits,fruit)
            scored=true
            sfx(fruit.sound)
        end
        
        if fruit.y>water_level then
            del(fruits,fruit)
            --points-=fruit.nscore
            player.lives-= 1
            scored=true
            local splash = {sp=230,x=fruit.x,y=fruit.y-7,ttl=25,count=0}
            add(splashes, splash)
            
           sfx(3)

        end
        update_splash()
    end
end

function fruit_draw()
    palt(colors.black,false)
    palt(colors.pink, true)
    for fruit in all(fruits) do
        spr(fruit.sprite,fruit.x,fruit.y,2,2)
    end    
    palt(colors.black,true)
    palt(colors.pink, false)
end

function update_splash()
    for i=1,#splashes do
        if splashes[i] ~= nil then
        
        splashes[i].count+=1

        if splashes[i].count == 5 then
            splashes[i].sp +=2 
        elseif splashes[i].count == 10 then
            splashes[i].sp +=2 
        elseif splashes[i].count == 15 then
            splashes[i].sp -= 2 
        elseif splashes[i].count == 20 then
            splashes[i].sp -=2
            
        end

        if splashes[i].count >= splashes[i].ttl then
            del(splashes,splashes[i])
        end
    end
    end
    
end

function draw_splash()
        palt(colors.black,false)
        palt(colors.pink,true)
    for i=1,#splashes do
        local splash=splashes[i]
        spr(splash.sp,splash.x,splash.y,2,2)

    end
    palt(colors.black,true)
    palt(colors.pink,false)
end
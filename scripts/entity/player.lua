function player_init()
    player = {
        sp = 64,
        flp = false,
        x = 64-8,
        y = 80,
        w = 16,
        h = 16,
        sp_w = 2,
        sp_h = 2,
        dx = 0,
        dy = 0,
        max_dx = 0,
        max_dy = 0,
        acc = 0.5,
        anim = 0,
        walking = false,
        sliding = false,
        gravity = 0.3,
        friction = 0.85,
        alpha = colors.storm,
        lives=5,
        max_lives=5,
        life_score=0,
        life_threshold=50    
    }

end

function player_update()
    player.dx *= player.friction
    if btn(⬅️) then
        player.dx -= player.acc
        player.walking = true
        player.flp = true
    elseif btn(➡️) then
        player.dx += player.acc
        player.walking = true
        player.flp = false
    elseif player.walking and not btn(⬅️) and not btn(➡️) then
        player.walking = false
        player.sliding = true
    elseif player.sliding then
        if abs(player.dx) < 0.2 or player.running then
            player.dx=0
            player.sliding = false
        end
    end

    player.x += player.dx

    --limit player to map
    if player.x < 0 then 
        player.x = 0
    end

    if player.x > 128-16 then
        player.x = 128-16
    end


end


function player_draw()
    player_animate()
    palt(colors.black,false)
    palt(player.alpha,true)
    pal(5,128,0)
    spr(player.sp,player.x,player.y,player.sp_w,player.sp_h,player.flp)
    pal()

    palt(colors.black,true)
    palt(player.alpha,false)
end

function player_animate()
    if player.walking then 
        if time() - player.anim > 0.1 then
            player.anim = time()
            player.sp += 2
            if player.sp > 72 then
                player.sp = 68
            end
        end
    else
        if time() - player.anim > 0.5 then 
            player.anim = time()
            player.sp += 2
            if player.sp > 66 then
                player.sp = 64
            end
        end
    end
end

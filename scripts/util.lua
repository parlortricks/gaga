    -- color table
    colors={
        black=      0,    --#000000
        storm=      1,    --#1D2B53
        wine=       2,    --#7E2553
        moss=       3,    --#008751
        tan=        4,    --#AB5236
        slate=      5,    --#5F574F
        silver=     6,    --#C2C3C7
        white=      7,    --#FFF1E8
        ember=      8,    --#FF004D
        orange=     9,    --#FFA300
        lemon=      10,   --#FFEC27
        lime=       11,   --#00E436
        sky=        12,   --#29ADFF
        dusk=       13,   --#83769C
        pink=       14,   --#FF77AB
        peach=      15    --#FFCCAA
    }

    secret_colors={
        cocoa=      128,  --#291814
        midnight=   129,  --#111D35
        port=       130,  --#422136
        sea=        131,  --#125359
        leather=    132,  --#742F29
        charcoal=   133,  --#49333B
        olive=      134,  --#A28879
        sand=       135,  --#F3EF7D
        crimson=    136,  --#BE1250
        amber=      137,  --#FF6C24
        tea=        138,  --#A8E72R
        jade=       139,  --#DDB543
        denim=      140,  --#065AB5
        aubergine=  141,  --#754665
        salmon=     142,  --#FF6E59
        coral=      143   --#FF9081
    }

    --button table
    button={
        left=       0,    --⬅️ left
        right=      1,    --➡️ right
        up=         2,    --⬆️ up
        down=       3,    --⬇️ down
        o=          4,    --🅾️ o/z
        x=          5     --❎ x
    }  

function get_last_played()
    year,month,day,hour,min,sec=dget(20),dget(21),dget(22),dget(23),dget(24),dget(25) 
    smonth,sday,shour,smin,ssec = 0,0,0,0,0
    if month < 10 then smonth = "0"..month else smonth = month end
    if day < 10 then sday = "0"..day else sday = day end
    if hour < 10 then shour = "0"..hour else shour = hour end
    if min < 10 then smin = "0"..min else smin = min end
    if sec < 10 then ssec = "0"..sec else ssec = sec end
    return ""..year.."-"..smonth.."-"..sday.." "..shour..":"..smin..":"..ssec
end

function save_last_played()
    --last played
    dset(20,stat(90)) --year
    dset(21,stat(91)) --month
    dset(22,stat(92)) --day
    dset(23,stat(93)) --hour
    dset(24,stat(94)) --min
    dset(25,stat(95)) --sec
end

function hcenter(s)
    -- screen center minus the
    -- string length times the 
    -- pixels in a char's width,
    -- cut in half
    return 64-#s*2
  end

  function hitbox_collide(ax1, ay1, ax2, ay2, bx1, by1, bx2, by2)
    local c1 = ay1 <= by2
    local c2 = ay2 >= by1
    local c3 = ax1 <= bx2
    local c4 = ax2 >= bx1
  
    if c1 and c2 and c3 and c4 then
      return true
    else
      return false
    end
  end

function to_items(e,n)
	for i=1,n do
		add(items,e)
	end
end

function from_items()
	return items[ceil(rnd(#items))]
end

-- inverse circle fill
-- note: centered at 64/64
-- r: radius
-- c: color
-- @freds72
function invcircfill(r,c)
    local r2=r*r
    color(c)
    for j=0,127 do
        local y=64-j
        local x=sqrt(max(r2-y*y))
        rectfill(0,j,64-x,j)
        rectfill(64+x,j,127,j)
    end
end

function fade_in()
    if fade then
        invcircfill(fade_count,0)
        fade_count+=4
    end    
end

function fade_out()
    if fade then
        invcircfill(fade_count,0)
        fade_count-=4 
    end       
end
function game_init()
    --variables
    _update = game_update
    _draw = game_draw
    water_level=112
    wave_wait=0
    new_wave=true
    game_over=false
  
    
 

    if (not scored)then
        waterfall_y=0
        river_x = 0
        river_y = 112
        bob=0        
        player_init()
    end   

    fruit_init()
    
    local c = cocreate(wave_banner)
    add(actions,c)
end

function game_update()

    
    bob+=.04

    if (not new_wave) then fruit_update() end
    
    player_update()
    waterfall_y+=0.25
    if waterfall_y > 127 then waterfall_y=0 end

    river_x+=0.25
    if river_x > 32 then river_x=0 end

    river_y+= sin(bob)/4

    life_update()
    


    if scored then
        if player.lives == 0 then
            game_over=true
            over_init()
        end    
    end

    if #fruits==0 and not game_over then
        level+=1

        game_init()
    end

    for c in all(actions) do
        if costatus(c) then
            coresume(c)
        else
            del(actions,c)
        end
    end

    if fade_count >= 90 then
        fade = false
    end
    update_splash()
    
end

function game_draw()
    cls()

    --draw cliff
    palt(colors.black,false)
    if level >= 50 then 
        pal(colors.tan,secret_colors.denim,0)
    elseif level >= 40 then
        pal()
        pal(colors.tan,secret_colors.moss,0)
    elseif level >= 30 then
        pal()
        pal(colors.tan,secret_colors.dusk,0)
    elseif level >= 20 then
        pal()
        pal(colors.tan,secret_colors.midnight,0)
    elseif level >= 10 then
        pal()
        pal(colors.tan,secret_colors.sea,0)
    end



    for x=0,15 do
        for y=0,15 do
            spr(rocks_table[x+1][y+1],x*16,y*16,2,2)
        end
    end

    pal()
    palt(colors.black,true)

    --draw waterfall
    if level >= 50 then 
        pal(colors.sky,secret_colors.amber,0)
    elseif level >= 40 then
        pal()
        pal(colors.sky,colors.ember,0)
    elseif level >= 30 then
        pal()
        pal(colors.sky,colors.lemon,0)
    elseif level >= 20 then
        pal()
        pal(colors.sky,secret_colors.salmon,0)
    elseif level >= 10 then
        pal()
        pal(colors.sky,secret_colors.coral,0)
    end

    map(0,0,48,waterfall_y-128,4,16)
    map(0,0,48,waterfall_y,4,16)
    pal()

    --draw bridge
    palt(colors.black,false)
    palt(colors.storm, true)

    for x=-32,112,48 do
        spr(192,x,96,6,4)
    end
    palt(colors.black,true)
    palt(colors.storm, false) 


    --draw river
    rectfill(0,120,127,127,colors.storm) --water


    spr(200,river_x-32,river_y,4,2) --waves
    spr(200,river_x,river_y,4,2)
    spr(200,river_x+32,river_y,4,2)
    spr(200,river_x+64,river_y,4,2)
    spr(200,river_x+96,river_y,4,2)
  

    --draw splash
    draw_splash()
    --draw fruit
    fruit_draw()

    --draw scoreboard
    scoreboard()

    --draw player
    player_draw()

    

    --draw wave
    if new_wave then
        local wavetxt = "\#5\^w\^twave "..level
        local wave_x_left = (64-#wavetxt*2) -2
        print(wavetxt,hcenter(wavetxt),40)
    end

    fade_in()

    
end

function wave_banner()
    for i=1,60 do
        yield()
    end
    new_wave=false
    
end

function scoreboard()
    rectfill(0,0,127,6,colors.slate)
    for i=0,player.max_lives-1 do
        print("♥",i*6,1,colors.dusk)
    end
    for i=0,player.lives-1 do
        print("♥",i*6,1,colors.ember)
    end
    local score_txt="score="..points
    print(score_txt,128/2-(#score_txt*4)/2,1,colors.lemon)
    local wave_txt="wave="..level
    print(wave_txt,128-#wave_txt*4,1,colors.lemon)
end

function life_update()
    if player.life_score >= player.life_threshold then
        player.lives += 1
        if player.lives > player.max_lives then
            player.lives = player.max_lives
        end  
        player.life_score -= player.life_threshold        
    end
end